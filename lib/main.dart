import 'package:flutter/material.dart';
import './textcontrol.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tugas UTS PEMROGRAMAN MOBILE',
      home: Scaffold(
        appBar: AppBar(
          title: Text('APLIKASI MOBILE'),
          backgroundColor: Colors.orange,
          centerTitle: true,
          leading: new IconButton(
            icon: new Icon(Icons.search),
            onPressed: () => {},
          ),
          // new Icon(Icons.menu),
          actions: <Widget>[
            new PopupMenuButton<Pilihan>(
            itemBuilder: (BuildContext context){
              return listPilihan.map((Pilihan x){
                return new PopupMenuItem<Pilihan>(
                  child: new Text(x.text),
                  value: x,
                );
              }).toList();
            },
          )

            // new IconButton(
            //   icon: new Icon(Icons.favorite_border),
            //   onPressed: () => {},
            // ),
            // new IconButton(
            //   icon: new Icon(Icons.search),
            //   onPressed: () => {},
            // ),
            // new IconButton(
            //   icon: new Icon(Icons.more_vert),
            //   onPressed: () => {},
            // ),
          ],
        ),
        body: TextControl(),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Pilihan{
  const Pilihan({this.text,this.warna});
  final String text;
  final Color warna;
}

List<Pilihan> listPilihan = const <Pilihan>[
  const Pilihan(text: "Android Studio", warna: Colors.red),
  const Pilihan(text: "Flutter", warna: Colors.red),
  const Pilihan(text: "Visual Studio Code", warna: Colors.red),
];