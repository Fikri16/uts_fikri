import 'package:flutter/material.dart';
import './textoutput.dart';

class TextControl extends StatefulWidget {
  @override
  _TextControlState createState() => _TextControlState();
}

class _TextControlState extends State<TextControl> {
  String msg = 'Android Studio';

  void _gantiText() {
    setState(() {
      if (msg.startsWith("F")) {
        msg = "Android Studio";
      } else if(msg.startsWith("V")){
        msg = "Flutter";
      } else {
        msg = "Visual Studio Code";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Aplikasi Pemrograman Mobile ',
            style: new TextStyle(fontSize: 25.0,color: Colors.orange),
          ),
          TextOutput(msg),
          RaisedButton(
            child: Text(
              'Ganti Aplikasi',
              style: new TextStyle(color: Colors.white),
            ),
            color: Colors.orange,
            onPressed: _gantiText,
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            splashColor: Colors.grey,
          ),
        ],
      ),
    );
  }
}
