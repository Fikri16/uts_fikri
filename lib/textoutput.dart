import 'package:flutter/material.dart';

class TextOutput extends StatelessWidget {
  final String nama;
  TextOutput(this.nama);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Text(
          '$nama',
          style: new TextStyle(fontSize: 35.0, color: Colors.orange),
        ),
      ),
    );
  }
}
